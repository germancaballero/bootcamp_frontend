// Interfaces en TS:
interface Mostrable
{
    // propiedad: number; SE PUEDE, PERO NO SE DEBE

    mostrar() : void;
}

// clases en TS:

class NumCuenta implements Mostrable {
    // VARIABLES MIEMBRO (Propiedades, atributos...)    
    iban: string;
    nombre: string;
    codSeguridad: number;
    foto: any;   // Tipo any es cualquier tipo de dato    
                 // No se debería usar NUNCA, o cuando no quede + remedio

    constructor(ib: string, nom: string, codSeg: number = 0, foto: any = null)
    {                                            // Con igual ponemos valor por defecto al parámetro
        this.iban = ib;                         
        this.nombre = nom;
        this.codSeguridad = codSeg;
        this.foto = foto;        
    }

    mostrar(): void {
        console.log(`Nombre: ${this.nombre} IBAN: ${this.iban} `);
    }
}
let miCuenta: NumCuenta;
miCuenta = new NumCuenta(null, null);
// miCuenta.cualquierProp = "sadfsdf";  Se puede? SÍ. Se debe? NO
// console.log("No deja de ser JS: " + miCuenta["cualquierProp"]);
miCuenta.iban = "ES-49234343445";
miCuenta.nombre = "Germán Caballero";
miCuenta.codSeguridad = 34;
miCuenta.foto = "32sdfg44";   // Any admite cualquier tipo
miCuenta.foto = false;        // boolean, string, number...
miCuenta.mostrar();

// Ejercicio: Hacer otra cuenta aleatoria
let otraCuenta: NumCuenta = new NumCuenta("ES-11111111", "Fulanito", 66);
otraCuenta.mostrar();
// Estado de una clase: se refiere al conjunto de valores que tiene una instancia (objeto) de una clase en un momento dado

// Ejercicio: Crear una clase llamada MaqCafe, 
// 3 variables miembro nivel -> núm, modelo -> string, activo -> booleano
// Métodos: Constructor: activo por defecto es falso, 
// Mostrar: Muestre toda la info, todo el estado
    // Máquina Cafetera Plus, Nivel 10, ACTIVA / DESACTIVA

//          ponerCafe(4): cuya salida es:          
//                  Cafetera Plus, nivel 4, poniendo café
//                  Café 25 %... Café 50 %... Café 75 %...Café 100 % Listo
// Crearos 2 instancias diferentes con ejemplos de llamadas tanto a mostrar() como a ponerCafe(N)

class MaqCafe /* implements Mostrable */ {  // Esta clase YA implementa Mostrable implícitamente
    // Manera reducida de crear variables, parámetros del constructor Y asignar el param con la prop.
    constructor(
        public nivel: number, 
        public modelo: string,  
        public activo: boolean = false) {
    }
    mostrar() : void {
        console.log(`Casfetera: ${this.modelo} nivel: ${this.nivel} `                        
                        + (this.activo ? "ACTIVO" : "DESACTIVO") );
    }
    ponerCafe(repeticiones: number): void {
        console.log(` ${this.modelo}, nivel ${this.nivel}, poniendo café`);
        for (let i: number = 1; i <= repeticiones; i++) {
            console.log(`Café ${100 / repeticiones * i} % ${ (i == repeticiones ? " Listo" : "")} ` );
        }
    }
}
let cafPlus:MaqCafe = new MaqCafe(10, "Cafetera Plus")
cafPlus.nivel = 12;
cafPlus.mostrar();
cafPlus.ponerCafe(4);

let cafExpress = new MaqCafe(5, "Caf. Express", true);
cafExpress.mostrar();
cafExpress.ponerCafe(5);

let arrayTodo: Mostrable[]; // Array<Mostrable>

arrayTodo = [miCuenta, 
    otraCuenta, 
    cafPlus, 
    cafExpress, 
    // "kkjkjdf"   // No se puede añadir otra cosa
    {
        mostrar:  function() { console.log("Hola, soy un obj.")}
    }
];

for (let index: number = 0; index < arrayTodo.length; index++) {
    const element = arrayTodo[index];    
    element.mostrar();
}
