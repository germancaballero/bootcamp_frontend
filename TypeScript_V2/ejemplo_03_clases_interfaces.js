// clases en TS:
var NumCuenta = /** @class */ (function () {
    // No se debería usar NUNCA, o cuando no quede + remedio
    function NumCuenta(ib, nom, codSeg, foto) {
        if (codSeg === void 0) { codSeg = 0; }
        if (foto === void 0) { foto = null; }
        this.iban = ib;
        this.nombre = nom;
        this.codSeguridad = codSeg;
        this.foto = foto;
    }
    NumCuenta.prototype.mostrar = function () {
        console.log("Nombre: " + this.nombre + " IBAN: " + this.iban + " ");
    };
    return NumCuenta;
}());
var miCuenta;
miCuenta = new NumCuenta(null, null);
// miCuenta.cualquierProp = "sadfsdf";  Se puede? SÍ. Se debe? NO
// console.log("No deja de ser JS: " + miCuenta["cualquierProp"]);
miCuenta.iban = "ES-49234343445";
miCuenta.nombre = "Germán Caballero";
miCuenta.codSeguridad = 34;
miCuenta.foto = "32sdfg44"; // Any admite cualquier tipo
miCuenta.foto = false; // boolean, string, number...
miCuenta.mostrar();
// Ejercicio: Hacer otra cuenta aleatoria
var otraCuenta = new NumCuenta("ES-11111111", "Fulanito", 66);
otraCuenta.mostrar();
// Estado de una clase: se refiere al conjunto de valores que tiene una instancia (objeto) de una clase en un momento dado
// Ejercicio: Crear una clase llamada MaqCafe, 
// 3 variables miembro nivel -> núm, modelo -> string, activo -> booleano
// Métodos: Constructor: activo por defecto es falso, 
// Mostrar: Muestre toda la info, todo el estado
// Máquina Cafetera Plus, Nivel 10, ACTIVA / DESACTIVA
//          ponerCafe(4): cuya salida es:          
//                  Cafetera Plus, nivel 4, poniendo café
//                  Café 25 %... Café 50 %... Café 75 %...Café 100 % Listo
// Crearos 2 instancias diferentes con ejemplos de llamadas tanto a mostrar() como a ponerCafe(N)
var MaqCafe /* implements Mostrable */ = /** @class */ (function () {
    // Manera reducida de crear variables, parámetros del constructor Y asignar el param con la prop.
    function MaqCafe(nivel, modelo, activo) {
        if (activo === void 0) { activo = false; }
        this.nivel = nivel;
        this.modelo = modelo;
        this.activo = activo;
    }
    MaqCafe.prototype.mostrar = function () {
        console.log("Casfetera: " + this.modelo + " nivel: " + this.nivel + " "
            + (this.activo ? "ACTIVO" : "DESACTIVO"));
    };
    MaqCafe.prototype.ponerCafe = function (repeticiones) {
        console.log(" " + this.modelo + ", nivel " + this.nivel + ", poniendo caf\u00E9");
        for (var i = 1; i <= repeticiones; i++) {
            console.log("Caf\u00E9 " + 100 / repeticiones * i + " % " + (i == repeticiones ? " Listo" : "") + " ");
        }
    };
    return MaqCafe;
}());
var cafPlus = new MaqCafe(10, "Cafetera Plus");
cafPlus.nivel = 12;
cafPlus.mostrar();
cafPlus.ponerCafe(4);
var cafExpress = new MaqCafe(5, "Caf. Express", true);
cafExpress.mostrar();
cafExpress.ponerCafe(5);
var arrayTodo; // Array<Mostrable>
arrayTodo = [miCuenta,
    otraCuenta,
    cafPlus,
    cafExpress,
    // "kkjkjdf"   // No se puede añadir otra cosa
    {
        mostrar: function () { console.log("Hola, soy un obj."); }
    }
];
for (var index = 0; index < arrayTodo.length; index++) {
    var element = arrayTodo[index];
    element.mostrar();
}
