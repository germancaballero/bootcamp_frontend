// TypeScript es JavaScript
let unaVariable = "Un texto"; // Infiere el tipo de dato del valor
// unaVariable = 200; MAL, CAMBIO DE TIPO
console.log("La variable contiene " + unaVariable);
// Se usa con el comando TSC: Type Script Compiler, que en realidad NO es un compilador, es más exacto que es un TRANSPILADOR. Si no dice nada, todo correcto
let numero: number = 111; // + "33"; No se puede mezclas de tipos
console.log("La variable contiene " + unaVariable + " y numero es " + numero);
let siValeCero: boolean;
siValeCero =   0 == 0;
if (siValeCero) {
    console.log("Hola, pues la condicion 0 == 0 es cierta");
}
let miObjeto : Object;
miObjeto = new Object();
if (miObjeto != null) {
    console.log("Hola, pues el objeto no es NULL");
}
let array : Array<number>;  // Los arrays obliga a que el todos los elementos 
                            // sean del mismo tipo
array = new Array<number>();
array[1] = 100; // OK
array[0] = 12.3434; // OK
// array[2] = "dfdfdf";    // No se puede (o más bien debe)
for (let i: number = 0; i < array.length; i++) {
    console.log("El elemento " + i + " vale " + array[i]);
}
var arrayJS = [2, "texto", false];  // Permite todo lo de JS

const VALOR_FIJO = 3.141592;    // Tanto let como const vienen de antes de ES6
let otroTexto: string;
otroTexto = "Adiós, ejemplo 01";
console.log(otroTexto);