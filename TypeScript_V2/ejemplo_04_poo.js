// Programación Orientada a Objetos
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var ReproductorGenerico = /** @class */ (function () {
    function ReproductorGenerico(modelo) {
        this.modelo = modelo;
    }
    ReproductorGenerico.prototype.play = function () { console.log(this.modelo + " Play"); };
    ReproductorGenerico.prototype.stop = function () { console.log(this.modelo + " Stop"); };
    ReproductorGenerico.prototype.pause = function () { console.log(this.modelo + " Pause"); };
    return ReproductorGenerico;
}());
var ReproductorMusica = /** @class */ (function (_super) {
    __extends(ReproductorMusica, _super);
    function ReproductorMusica(modelo, cancion) {
        var _this = _super.call(this, modelo) || this;
        _this.cancion = cancion;
        return _this;
    }
    ReproductorMusica.prototype.getModelo = function () {
        return this.modelo;
    };
    ReproductorMusica.prototype.setModelo = function (m) {
        this.modelo = m;
    };
    return ReproductorMusica;
}(ReproductorGenerico));
var Radio = /** @class */ (function (_super) {
    __extends(Radio, _super);
    function Radio(modelo, cancion, canal) {
        var _this = _super.call(this, modelo, cancion) || this;
        _this.canal = canal;
        return _this;
    }
    Radio.prototype.getCanal = function () {
        return this.canal;
    };
    return Radio;
}(ReproductorMusica));
var ReproductorVideo = /** @class */ (function (_super) {
    __extends(ReproductorVideo, _super);
    function ReproductorVideo(modelo, video) {
        var _this = _super.call(this, modelo) || this;
        _this.video = video;
        return _this;
    }
    ReproductorVideo.prototype.repVideo = function () {
        console.log("El modelo de reproduccion es " + this.modelo + " y el video es " + this.video);
    };
    return ReproductorVideo;
}(ReproductorGenerico));
var spotify = new ReproductorMusica("Rep. Spotify", "Welcome to the jungle");
spotify.play();
spotify.stop();
var radio = new Radio("FM", "Safaera", "107.1");
radio.play();
var video = new ReproductorVideo("VLC", "gatos");
video.repVideo();
// Ejercicio: Hacer Radio que herede de ReproductorMusica, añadiendo la propiedad "canal"
//Otra ReproductorVideo, que herede de ReproductorGenerico, añadiendo la propiedad "video"
// un método en RepVideo que muestre el modelo y el video que está reproduciendo.
var repMus = new ReproductorMusica("Rep. MP3", "Welcome to the jungle");
repMus.play();
// ReproductorMusica hereda de Generico, pero NO de Radio
var spotifyGen;
spotifyGen = spotify; // Puede tener la forma del padre
// en la clase hija sí accedemos al modelo, 
console.log("Modelo spotify: " + spotify.getModelo());
// pero con la forma del padre, NO:
// console.log("Modelo: " + spotifyGen.getModelo());
spotify.setModelo("Spotify PREMIUM");
console.log("Modelo spotifyGen: " + spotifyGen.getModelo());
var x = 10;
var y = x;
x = 5;
console.log("X = " + x + " Y = " + y);
