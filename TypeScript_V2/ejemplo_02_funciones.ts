// Funciones en TS y tipo void

function sumar(x: number, y: number) : number {
    return x + y;
}
console.log("Resultado suma: " + sumar(10, 20));
// console.log("Resultado concatenacion: " + sumar("10", "20")); ERROR de tipo
// Ejercio 1: hacer funcion de concatenar, que reciba 2 strings y devuelva 1 string
// Ejercio 2: hacer funcion de comparar, que reciba 2 booleanos y devuelva 1 booleano

function concatenar(x: string, y: string) : string {
    return x + y;
}
console.log("Resultado concatenar: " + concatenar("10", "20"));

function compararAND(x: boolean, y: boolean) : boolean {
    return x && y;
}
function compararIGUAL(x: boolean, y: boolean) : boolean {
    return x == y;
}
console.log("Resultado comparar: " + compararAND(false, false));
console.log("Resultado comparar: " + compararIGUAL(false, false));

// Otra manera de declarar arrays es con  tipo_dato[]
let mostrarArrayNum = function (unArray : number[]) : void
{
    if (unArray == null)    return;
    if (typeof unArray === "undefined") console.log("SIN DEFINIR");

    for (let i: number = 0; i < unArray.length; i++) {
        console.log(`Elem ${i} es ${unArray[i]}`);
    }
    // return "Cualquier cosa"; MAL, void no debe devolver nada
}
let miArray: Array<number> = [10, 20, 30];
let arrayMalo: Array<number>;
mostrarArrayNum(arrayMalo);
mostrarArrayNum(null);
mostrarArrayNum(miArray);