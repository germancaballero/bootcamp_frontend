// Programación Orientada a Objetos

/* 
 - 1ª: Clases  -> Objetos       (JS no tiene clases, tiene prototipos)
 - 2ª: Encapsulación: Métodos privados, no accesibles fuera de la clase
                      Métodos protegidos, pueden ser usados en su clase y clases herederas
                      Métodos públicos, accesibles fuera de la clase
                (*) JS no tiene encapsulación de manera nativa
                REGLA DE ORO: Siempre propiedades privadas a menos que no quede más remedio de hacerlas protected o públicas
 - 3ª: Herencia: Una clase puede heredar de otra, 
                 Hereda TODO: sus métodos y sus variables miembro
                 La mayoría de lenguajes (JS, TS, PHP...) NO tienen
                    herencia múltiple, es decir, una clase sólo puede
                    heredar de otra.
                (*) JS en realidad no tiene herencia de manera  nativa
 - 4ª: Polimorfismo: Un objeto de una clase puede tener su forma o la forma de cualquiera de sus clases padres, nunca de una clase hijo
                También aplicable a interfaces
                (*) JS no tiene polimorfismo. Es dinámico, por lo que una variable puede cambiar de tipo, pero no tiene polimorfismo.
JavaScript no termina de ser orientada a objetos real.
TypeScript SÍ intenta (no consigue) ser Orientado a Objetos...
    ... porque TypeScript es JavaScript extendido.
 */

interface Reproducible {
    play(): void;
    stop(): void;
    pause(): void;
}
interface Grabable {
    grabar(): void;
}
abstract class ReproductorGenerico implements Reproducible {
    constructor(protected modelo: string) { }
    public play(): void { console.log(`${this.modelo} Play`); }
    public stop(): void { console.log(`${this.modelo} Stop`); }
    public pause(): void { console.log(`${this.modelo} Pause`); }
}
class ReproductorMusica extends ReproductorGenerico {
    constructor(modelo: string, private cancion: string) {
        super(modelo);  // Invocamos al constructor del padre
    }
    public getModelo(): string {
        return this.modelo;
    }    
    public setModelo(m: string): void {
        this.modelo = m;
    }    
}
class Radio extends ReproductorMusica {
    constructor(modelo: string, cancion: string, private canal: string) {
        super(modelo, cancion);
    }
    public getCanal(): string {
        return this.canal;
    }
}

class ReproductorVideo extends ReproductorGenerico {
    constructor(modelo: string, private video: string) {
        super(modelo);
    }

    repVideo(): void {
        console.log(`El modelo de reproduccion es ${this.modelo} y el video es ${this.video}`);
    }
}

let spotify = new ReproductorMusica("Rep. Spotify", "Welcome to the jungle");
spotify.play();
spotify.stop();
let radio = new Radio("FM", "Safaera", "107.1");
radio.play();

let video = new ReproductorVideo("VLC", "gatos");
video.repVideo();
// Ejercicio: Hacer Radio que herede de ReproductorMusica, añadiendo la propiedad "canal"
//Otra ReproductorVideo, que herede de ReproductorGenerico, añadiendo la propiedad "video"
// un método en RepVideo que muestre el modelo y el video que está reproduciendo.
let repMus = new ReproductorMusica("Rep. MP3", "Welcome to the jungle");
repMus.play();

// ReproductorMusica hereda de Generico, pero NO de Radio
let spotifyGen: ReproductorGenerico;
spotifyGen = spotify;   // Puede tener la forma del padre
// en la clase hija sí accedemos al modelo, 
console.log("Modelo spotify: " + spotify.getModelo());
// pero con la forma del padre, NO:
// console.log("Modelo: " + spotifyGen.getModelo());
spotify.setModelo("Spotify PREMIUM");
// console.log("Modelo spotifyGen: " + spotifyGen.getModelo());


let x = 10;
let y = x;
x = 5;
console.log(`X = ${x} Y = ${ y}`)
