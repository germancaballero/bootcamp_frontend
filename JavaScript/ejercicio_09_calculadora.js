// Ecma Script 5 es la estandarización de JavaScript desde los 2000
// Ecma Script 6 es la <<>> de JS desde el 2015. También ES2015 - ES6
// Incluye soporte para clases, template strings, pero ES LO MISMO...
// Mientras que var tiene ámbito de función, let lo tiene de bloque
class Boton {

    constructor(id, valor, parent) {
        let input = document.createElement("input");
        input.value = valor;
        input.id = id;
        input.type = "button";
        parent.appendChild(input);
        this.input = input;
    }
}
