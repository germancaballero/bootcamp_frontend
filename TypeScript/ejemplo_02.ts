// Todo objeto que implemente una interfaz debe tener sus métodos y propiedades
interface Mostrable {
    mostrar(): void;    
}
class NumCuenta implements Mostrable {
    iban: string;
    nombre: string;
    codSeguridad: any;

    constructor(ib: string, nom: string) {
        this.iban = ib;
        this.nombre = nom;
    }
    mostrar(): void {
        console.log(`Info cuenta: ${this.iban} - ${this.nombre} `);
    }
}
// Parte 1 del programa
let objCuenta: NumCuenta = new NumCuenta("123 00", "Fulano");

// Parte 2 del programa
class MaqCafe implements Mostrable {
    nivel: number;
    constructor(n : number) {
        this.nivel = n;
    }
    mostrar(): void {
        console.log(`Máq café: ${this.nivel} `);
    }
}
let maquinaCafe: MaqCafe = new MaqCafe(90);
// Parte 3 del programa
objCuenta.mostrar();
maquinaCafe.mostrar();