console.log("TypeScript es JavaScript");
var CON_CONSTANTES = "Introdujo const antes que EcmaScript 2015";
console.log(CON_CONSTANTES);
// la siguiente variable es de tipo texto (string) porque
// Se infiere (deduce) el tipo de dato de su valor
var variable = "Let se introdujo en TS";
console.log(variable);
variable = "100";
console.log(variable);
