// Encapsulación y herencia

abstract class Vehiculo {

    public encendido: boolean = false;

    // Recibir parámetros, declarar variables miembro y la asignación en un 3 x 1
    constructor( private numRuedas: number, 
                 public modelo: string) {
        this.encendido = false;
    }

    // El método arrancar depende de la clase concreta y debe implementarse
    // para poder instanciar un objeto
    public abstract arrancar() : void;

    // Método por defecto, se puede re-implementar en la clase hija
    public frenar() : void {
        if (this.encendido)
            console.log("Frenar al menos como los Picapiedra");
        else
            console.log("Ya estás parado");
    }
    public encender(nuevoEstado: boolean) {
        this.encendido = nuevoEstado;
    }
}
abstract class VehMaritimo extends Vehiculo {

}
class Coche extends Vehiculo {
    public gasolina: number = 50;

    constructor(numRu: number, mod: string, gaso: number) {
        super(numRu, mod);
        // Vehiculo.call(this);
        this.gasolina = gaso;
    }

    public arrancar(): void {
        if ( ! this.encendido) {
            this.encendido = true;
            console.log("Brummm brumm!");
        }
    }
    public acelerar(): void {
        if ( this.encendido && this.gasolina > 0) {
            console.log("BRUUUUUUUUUUUUUM!");
            this.gasolina -= 0.1; 
        }
    }
    sinImplementar(): void {
        // Provocamos una excepción (error en tiempo de ejecución )
        throw new Error("Method not implemented.");
    }
    frenar(): void {
        console.log("Frenar normal");
    }
}
let miBuga = new Coche(4, "Lamborghini Diablo", 60);

try {
    miBuga.encender(true);
    miBuga.arrancar();
    miBuga.acelerar();
    miBuga.encendido = true;
    miBuga.acelerar();
    miBuga.acelerar();
    
    miBuga.frenar();
    console.log("Queda " + miBuga.gasolina + " litros gas");
} catch (err) {
    console.log("Capturamos el error y seguimos")
}
console.log("Seguimos");


// Ejercicio: Jerarquía de clases e interfaces:
interface IElectrico {
     cargar(): void;
}
interface IVolador {
    volar(): void;
}
// ¿Herencia de Coche o Vehiculo?
// De Vehiculo no tenemos la gasolina pero hay que programarlo todo
// De Coche reutilizamos el código pero tenemos propiedades de sobra
class CocheElectrico extends Coche implements IElectrico {
    bateria: number;

    constructor(numRu: number, mod: string, bateria: number) {
        super(numRu, mod, 0);   // Sin gasolina
        // Vehiculo.call(this);
        this.bateria = bateria;
    }
    cargar(): void {
        this.bateria = 100;
        console.log("Coche cargado");
    }
    public acelerar(): void {
        if ( this.encendido && this.bateria > 0) {
            console.log("Coche electrico acelera");
            this.bateria -= 0.2; 
        }
    }
}
let miTesla = new CocheElectrico(4, "Tesla", 100);
miTesla.arrancar();
miTesla.acelerar();
miTesla.frenar();

class Avion extends Vehiculo implements IVolador {
    public arrancar(): void {
        console.log("Arranco avion");
    }
    volar(): void {
        console.log("Vuela avionsito vuela");
    }   
}
class AvionElectrico extends Avion implements IElectrico /*, IVolador */ {
    
    cargar(): void {
        throw new Error("Carga ");
    }
}
let vehiculos: Vehiculo[] = new Array();
let electricos: IElectrico[] = new Array();

let avionsito = new AvionElectrico(20, "Airbus");

vehiculos.push(miTesla);
vehiculos.push(miBuga);
vehiculos.push(avionsito);
electricos.push(miTesla);
// electricos.push(miBuga);

for (let veh of vehiculos) {
    veh.arrancar();
    veh.frenar();
}