function validarNombre() {
    let inputNombre = document.getElementById("nombre");
    let errorNombre = document.getElementById("errorNombre");

    if (inputNombre.value.length < 2) {
        inputNombre.setAttribute("class", "form-input-invalid");
        errorNombre.style.display = "block";
        errorNombre.innerHTML = "El nombre debe tener más de 2 letras";
        return false;
    } else {
        inputNombre.className = "form-input-valid";
        errorNombre.style.display = "none";
        return true;
    }
}
function validarEmail() {

    let inputEmail = document.getElementById("email");
    let posicionPunto = inputEmail.value.lastIndexOf(".");
    if (posicionPunto < 0) {
        inputEmail.setAttribute("class", "form-input form-input-invalid");
    } else {
        let posArroba = inputEmail.value.lastIndexOf("@");
        if (posArroba >= posicionPunto) {
            inputEmail.setAttribute("class", "form-input-invalid");
        } else {

            if (posicionPunto >= inputEmail.value.length - 2
                || posicionPunto < inputEmail.value.length - 5) {
                inputEmail.setAttribute("class", "form-input-invalid");
            } else {

                inputEmail.setAttribute("class", "form-input-valid");
            }
        }
    }
}
function validarTelefono() {
    let inputTlfn = document.getElementById("tlfn");
    let errorTlfn = document.getElementById("errorTlfn");
    let objExpReg = new RegExp("^[0-9]{6,9}$");
    if (  ! objExpReg.test(inputTlfn.value)) {
        inputTlfn.setAttribute("class", "form-input form-input-invalid");
        inputTlfn.style.display = "block";
        errorTlfn.innerHTML = "El teléfono deben ser de 6 a 9 dígitos";
        return false;
    } else {
        inputTlfn.className = "form-input form-input-valid";
        errorTlfn.style.display = "none";
        return true;
    }
}
function validarAlEnviar() {
    try {   // Bloque a vigilar que no haya excepciones
        let resultado = validarNombre() && validarTelefono();
        validarEmail();
        return resultado;
    } catch (ex) {   // Si salta una excepcion
        console.error(ex);
        return false;
    } finally { // Siempre se ejecuta
        // return false;
    }
}

window.onload = function () {
    document.forms.registro.onsubmit = validarAlEnviar;

    // Al perder el foco
    document.getElementById("nombre").onblur = validarNombre;
    document.getElementById("nombre").onkeyup = validarNombre;
    document.getElementById("email").onblur = validarEmail;
    document.getElementById("tlfn").onblur = validarTelefono;

    let selectExt = document.getElementById("extensionTlfn");
    let extPais = [{ "valor": "+34", texto: "España" },
    { valor: "+32", 'texto': "Portugal" },
    { valor: "+31", texto: "Italia" }];
    let extFrancia = new Object();  // Lo mismo de otra manera
    extFrancia.valor = "+39";
    extFrancia.texto = "Francia";
    extPais.push(extFrancia);

    for (let i = 0; i < extPais.length; i++) {
        let option = document.createElement("option");
        option.value = extPais[i].valor;
        option.innerHTML = extPais[i].texto;
        selectExt.appendChild(option);
    }

}